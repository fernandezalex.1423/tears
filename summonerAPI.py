from riotwatcher import LolWatcher, ApiError

f = open("riotKey", "r")
key = f.read()
watcher = LolWatcher(key)
region = 'na1'


def retrieve_summoner(summonerName):  # this returns a dictionary with player info
    summoner = watcher.summoner.by_name(region, summonerName)
    return summoner


def summoner_icon(summonerName):
    summoner = watcher.summoner.by_name(region, summonerName)
    return summoner['profileIconId']


def summoner_level(summonerName):
    summoner = watcher.summoner.by_name(region, summonerName)
    return summoner['summonerLevel']


def ranked_information(summonerName):  # dictionary containing player ranked information
    ranked_stats = watcher.league.by_summoner(region, retrieve_summoner(summonerName)['id'])
    return ranked_stats


def match_history_blue(
        summonerName):  # list of matches, each list element contains a dictionary with player match history
    matches = watcher.match.matchlist_by_puuid("americas", retrieve_summoner(summonerName)['puuid'])
    lobby = []
    for i in range(9):
        last_match = matches[i]
        match_detail = watcher.match.by_id("americas", last_match)
        players = []
        reset = 0
        for row in match_detail['info']['participants']:
            if row['teamId'] == 100:
                participants_row = {}
                participants_row['champion'] = row['championName']
                participants_row['spell1'] = row['summoner1Id']
                participants_row['spell2'] = row['summoner2Id']
                participants_row['win'] = row['win']
                participants_row['kills'] = row['kills']
                participants_row['deaths'] = row['deaths']
                participants_row['assists'] = row['assists']
                participants_row['totalDamageDealt'] = row['totalDamageDealt']
                participants_row['goldEarned'] = row['goldEarned']
                participants_row['champLevel'] = row['champLevel']
                participants_row['totalMinionsKilled'] = row['totalMinionsKilled']
                participants_row['item0'] = row['item0']
                participants_row['item1'] = row['item1']
                participants_row['summonerName'] = row['summonerName']
                participants_row['teamId'] = row['teamId']
                players.append(participants_row)
            reset += 1
            if reset == 9:
                lobby.append(players)
    return lobby


def match_history_red(summonerName):  # list of matches, each element contains a dictionary with player match history
    matches = watcher.match.matchlist_by_puuid("americas", retrieve_summoner(summonerName)['puuid'])
    lobby = []
    for i in range(9):
        last_match = matches[i]
        match_detail = watcher.match.by_id("americas", last_match)
        players = []
        reset = 0
        for row in match_detail['info']['participants']:
            if row['teamId'] == 200:
                participants_row = {}
                participants_row['champion'] = row['championName']
                participants_row['spell1'] = row['summoner1Id']
                participants_row['spell2'] = row['summoner2Id']
                participants_row['win'] = row['win']
                participants_row['kills'] = row['kills']
                participants_row['deaths'] = row['deaths']
                participants_row['assists'] = row['assists']
                participants_row['totalDamageDealt'] = row['totalDamageDealt']
                participants_row['goldEarned'] = row['goldEarned']
                participants_row['champLevel'] = row['champLevel']
                participants_row['totalMinionsKilled'] = row['totalMinionsKilled']
                participants_row['item0'] = row['item0']
                participants_row['item1'] = row['item1']
                participants_row['summonerName'] = row['summonerName']
                participants_row['teamId'] = row['teamId']
                players.append(participants_row)
            reset += 1
            if reset == 9:
                lobby.append(players)
    return lobby


ranked = ranked_information('Irucin')
# print(ranked)
test = ranked[0]['queueType']
test1 = 'RANKED_SOLO_5x5'
test2 = 'RANKED_SOLO_5x5'
if test is test2:
    print("this thing actually works ffs")
elif ranked[0]['queueType'] == 'RANKED_FLEX_SR':
    print("hsfldgsdkgsdgk")

print(ranked[0]['queueType'])

