from flask import Flask, render_template, request
import summonerAPI

app = Flask(__name__)


@app.route('/')
def main():
    return render_template("main.html")


@app.route('/summoner', methods=['GET', 'POST'])
def summoner_request():
    summoner_name = request.form['summoner_name']
    icon_string = str(summonerAPI.summoner_icon(summoner_name))
    return render_template('summoner page.html',
                           player_name=summoner_name,
                           icon_id="static/dragontail-12.10.1/12.10.1/img/profileicon/" + icon_string + ".png",
                           summoner_level=summonerAPI.summoner_level(summoner_name),
                           ranked_information=summonerAPI.ranked_information(summoner_name),
                           match_history_blue=summonerAPI.match_history_blue(summoner_name),
                           match_history_red=summonerAPI.match_history_red(summoner_name)
                           )


if __name__ == '__main__':
    app.debug = True
    app.run(debug=True)


def _init_(self, player_name):
    self.player_name = player_name
