# Tears

An OP.GG inspired webpage clone.

This project makes use of RiotAPI through a thin API wrapper known as RiotWatcher

Tears.gg is developed using the Flask web framework and jinja2 template engine

app.py renders the actual webpage and calls on the html templates

summonerAPI.py calls on RiotAPI through RiotWatcher and pulls player data

templates contains main.html and summonerpage.html, these files render the content
onto the web.